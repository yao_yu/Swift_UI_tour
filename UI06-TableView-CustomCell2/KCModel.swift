//
//  KCModel.swift
//  UI06-TableView-CustomCell2
//
//  Created by yao_yu on 14-8-25.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import Foundation
import UIKit

func KCColor(r: CGFloat, g: CGFloat, b: CGFloat) -> UIColor{
    return UIColor(hue: r / 255.0, saturation: g / 255.0, brightness: b / 255.0, alpha: 1)//颜色宏定义
}
let kStatusTableViewCellControlSpacing:CGFloat = 10 //控件间距
let kStatusTableViewCellBackgroundColor = KCColor(251,251,251)
let kStatusGrayColor = KCColor(50,50,50)
let kStatusLightGrayColor = KCColor(120,120,120)

let kStatusTableViewCellAvatarWidth:CGFloat = 40 //头像宽度
let kStatusTableViewCellAvatarHeight = kStatusTableViewCellAvatarWidth
let kStatusTableViewCellUserNameFontSize:CGFloat = 14
let kStatusTableViewCellMbTypeWidth:CGFloat = 13 //会员图标宽度
let kStatusTableViewCellMbTypeHeight = kStatusTableViewCellMbTypeWidth
let kStatusTableViewCellCreateAtFontSize:CGFloat = 12
let kStatusTableViewCellSourceFontSize:CGFloat = 12
let kStatusTableViewCellTextFontSize:CGFloat = 14

class KCStatus{
    var Id = 0
    var profileImageUrl = ""
    var userName = ""
    var mbtype = ""
    var createdAt = ""
    var _source = ""
    var text = ""
    
    var source:String{
        return "来自\(_source)"
    }
    
    init(dict:[String:String]){
        self.Id = dict["Id"]!.toInt()!
        self.profileImageUrl = dict["profileImageUrl"]!
        self.userName = dict["userName"]!
        self.mbtype = dict["mbtype"]!
        self.createdAt = dict["createdAt"]!
        self._source = dict["source"]!
        self.text = dict["text"]!
    }
}

class KCStatusTableViewCell:UITableViewCell{
    var status:KCStatus!
    var height:CGFloat = 0
    
    var headImage:UIImageView = UIImageView()
    var mbType:UIImageView = UIImageView()
    var userName:UILabel = UILabel()
    var createAt:UILabel = UILabel()
    var source:UILabel = UILabel()
    var caption:UILabel = UILabel()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String!) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.initSubView()
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func initSubView(){
        self.addSubview(headImage)
        userName.textColor = kStatusGrayColor
        userName.font = UIFont.systemFontOfSize(kStatusTableViewCellUserNameFontSize)
        self.addSubview(userName)
        self.addSubview(mbType)
        createAt.textColor = kStatusLightGrayColor
        createAt.font = UIFont.systemFontOfSize(kStatusTableViewCellCreateAtFontSize)
        self.addSubview(createAt)
        source.textColor = kStatusLightGrayColor
        source.font = UIFont.systemFontOfSize(kStatusTableViewCellSourceFontSize)
        self.addSubview(source)
        caption.textColor = kStatusGrayColor
        caption.font = UIFont.systemFontOfSize(kStatusTableViewCellTextFontSize)
        caption.numberOfLines = 0
        self.addSubview(caption)
    }
    
    func setStatus(status:KCStatus){
        // 头像大小和位置
        var dx:CGFloat = 10, dy:CGFloat = 10
        var aRect = CGRectMake(dx, dy, kStatusTableViewCellAvatarWidth, kStatusTableViewCellAvatarHeight)
//        headImage.image
        headImage.frame = aRect
        
        //会员图标
        let userNameX = CGRectGetMaxX(aRect) + kStatusTableViewCellControlSpacing
        let userNameY =  dy
    }
    
}


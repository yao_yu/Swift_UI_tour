//
//  ViewController.swift
//  UI06-TableView-CustomCell2
//
//  Created by yao_yu on 14-8-25.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var tableView = UITableView()
    
                            
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
    }
    
    func setupViews(){
        
        view.backgroundColor = UIColor.blackColor()
        
        tableView.setTranslatesAutoresizingMaskIntoConstraints(false)
        let childViews = ["tableView":tableView]
        view.addSubview(tableView)
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("|[tableView]|", options: NSLayoutFormatOptions(0), metrics: nil, views: childViews))
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-20-[tableView]|", options: NSLayoutFormatOptions(0), metrics: nil, views: childViews))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
}


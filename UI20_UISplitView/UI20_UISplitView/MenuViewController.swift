//
//  MenuViewController.swift
//  UI20_UISplitView
//
//  Created by yao_yu on 14-9-24.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import UIKit

protocol MonsterSelectionDelegate{
    func selectedMonster(monster:(String,String,String,String))
}

class MenuViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    var monsterDelegate: MonsterSelectionDelegate?
    
    var menuItems = [(String,String,String,String)]()
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadMenus()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self
        tableView.delegate = self

    }
    
    func loadMenus(){
        var index = 0
        menuItems = [
            ("Cat-Bot" ,"MEE-OW"  ,"meetcatbot.png" ,"Sword"),
            ("Dog-Bot" ,"BOW-WOW" ,"meetdogbot.png" ,"Blowgun"),
            ("Explode-Bot" ,"Tick, tick, BOOM!" ,"meetexplodebot.png" ,"Smoke"),
            ("Fire-Bot" ,"Will Make You Steamed" ,"meetfirebot.png" ,"NinjaStar"),
            ("Ice-Bot" ,"Has A Chilling Effect" ,"meeticebot.png" ,"Fire"),
            ("Mini-Tomato-Bot" ,"Extremely Handsome" ,"meetminitomatobot.png" ,"NinjaStar")
        ]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("Cell") as UITableViewCell
        
        let (title, detail, name, _) = menuItems[indexPath.row]
        cell.textLabel?.text = title
        cell.detailTextLabel?.text = detail
        cell.imageView?.image = UIImage(named: name + ".png")
        return cell
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if monsterDelegate != nil{
            monsterDelegate?.selectedMonster(menuItems[indexPath.row])
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

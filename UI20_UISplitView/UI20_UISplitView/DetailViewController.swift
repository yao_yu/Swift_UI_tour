//
//  DetailViewController.swift
//  UI20_UISplitView
//
//  Created by yao_yu on 14-9-24.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, MonsterSelectionDelegate, UISplitViewControllerDelegate {

    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imageA: UIImageView!
    @IBOutlet weak var imageWeapon: UIImageView!
    @IBOutlet weak var navItem: UINavigationItem!

    var popover:UIPopoverController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func splitViewController(svc: UISplitViewController, willHideViewController aViewController: UIViewController, withBarButtonItem barButtonItem: UIBarButtonItem, forPopoverController pc: UIPopoverController) {
        
        self.popover = pc
        barButtonItem.title = "怪物"
        navItem.setLeftBarButtonItem(barButtonItem, animated: true)
    }
    
    func splitViewController(svc: UISplitViewController, willShowViewController aViewController: UIViewController, invalidatingBarButtonItem barButtonItem: UIBarButtonItem) {
        navItem.setLeftBarButtonItem(nil, animated: true)
        popover = nil
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func selectedMonster(monster: (String, String, String, String)) {
        let (title, detail, mname, wname) = monster
        
        navItem.title = title
        lblName.text = title
        lblDetail.text = detail
        imageA.image = UIImage(named: mname + ".png")
        imageWeapon.image = UIImage(named: wname + ".png")
        
        if popover != nil{
            popover?.dismissPopoverAnimated(true)
        }
    }
}

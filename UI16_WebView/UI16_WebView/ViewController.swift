//
//  ViewController.swift
//  UI16_WebView
//
//  Created by yao_yu on 14-9-24.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var webview:UIWebView!
    @IBOutlet weak var okButton:UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        okButton.addTarget(self, action: "onOK", forControlEvents: UIControlEvents.TouchUpInside)
        

//        webView.loadRequest(NSURLRequest(URL: NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("index", ofType: ".htm")!, encoding: NSUTF8StringEncoding, error: nil)))
        let file = NSBundle.mainBundle().pathForResource("index", ofType: ".htm")
        let url = NSURL(fileURLWithPath: file!)
        webview.loadRequest(NSURLRequest(URL: url))
        
        webview.stringByEvaluatingJavaScriptFromString("var script = document.createElement('script');" +
        "script.type = 'text/javascript';" +
        "script.text = \"function myFunction() { " +
        "var field = document.getElementsByName('word')[0];" +
        "field.value='测试';" +
        "document.forms[0].submit();" +
        "}\";" +
        "document.getElementsByTagName('head')[0].appendChild(script);")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func onOK(){
        webview.stringByEvaluatingJavaScriptFromString("display_alert();")

    }

}


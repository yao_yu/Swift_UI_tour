//
//  NJWeibo.swift
//  UI09-TableView-CustomCell3
//
//  Created by yao_yu on 14-8-28.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import Foundation
import UIKit

class NJWeibo:NSObject{
    var text = ""
    var icon = ""
    var name = ""
    var picture = ""
    var vip = false
    
    init(dict:NSDictionary) {
        super.init()
        self.setValuesForKeysWithDictionary(dict)
    }
}

let NJNameFont = UIFont.systemFontOfSize(15)
let NJTextFont = UIFont.systemFontOfSize(16)

class NJWeiboCell:UITableViewCell{
    var iconView:UIImageView!
    var vipView:UIImageView!
    var pictureView:UIImageView!
    var nameLabel:UILabel!
    var introLabel:UILabel!
    
    class func cellWithTableView(tableView:UITableView) -> NJWeiboCell{
        let identifier = "status"
        var cell = tableView.dequeueReusableCellWithIdentifier(identifier) as NJWeiboCell?
        if let _cell = cell{
            cell = NJWeiboCell(style: UITableViewCellStyle.Default, reuseIdentifier: identifier)
        }
        return cell!
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String!) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        //头像
        iconView = UIImageView(frame: CGRectMake(0, 0, 44, 44))
        iconView.setTranslatesAutoresizingMaskIntoConstraints(false)
        contentView.addSubview(iconView)
        
        // 昵称
        nameLabel = UILabel(frame: CGRectMake(0, 0, 200, 22))
        nameLabel.font = NJNameFont
        nameLabel.setTranslatesAutoresizingMaskIntoConstraints(false)
        contentView.addSubview(nameLabel)
        
        // vip
        vipView = UIImageView(frame: CGRectMake(0, 0, 22, 22))
        vipView.image = UIImage(named: "vip")
        vipView.setTranslatesAutoresizingMaskIntoConstraints(false)
        contentView.addSubview(vipView)
        
        // 正文
        introLabel = UILabel()
        introLabel.font = NJTextFont
        introLabel.numberOfLines = 0
        introLabel.setTranslatesAutoresizingMaskIntoConstraints(false)
        contentView.addSubview(introLabel)
        
        // 配图
        pictureView = UIImageView()
        pictureView.setTranslatesAutoresizingMaskIntoConstraints(false)
        contentView.addSubview(pictureView)
        
        var views:[String:UIView] = ["icon":iconView, "name":nameLabel, "vip":vipView, "intro":introLabel, "pic":pictureView]
        contentView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("|[icon]-[name]-[vip]", options: NSLayoutFormatOptions(0), metrics: nil, views: views))
        contentView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("|[intro]|", options: NSLayoutFormatOptions(0), metrics: nil, views: views))
        contentView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("|[pic]|", options: NSLayoutFormatOptions(0), metrics: nil, views: views))
        contentView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[icon]-[intro]-[pic]|", options: NSLayoutFormatOptions(0), metrics: nil, views: views))
    }

    func setData(weibo:NJWeibo){
        iconView.image = UIImage(named: weibo.icon)
        nameLabel.text = weibo.name
        vipView.hidden = !weibo.vip
        introLabel.text = weibo.text
        pictureView.image = UIImage(named: weibo.picture)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
}
//
//  ViewControllerA.swift
//  UI12-TabbarController
//
//  Created by yao_yu on 14-9-3.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import UIKit

class ViewControllerA: UIViewController {
    
    var _file = ""
    
    class func createNavigationController (_title:String, _ _file:String)-> UINavigationController{
        let vc = ViewControllerA()
        vc.title = _title
        vc._file = _file
        let nav = UINavigationController(rootViewController: vc)
        let img = UIImage(named: "\(_file)_h@2x.png")
        let item = UITabBarItem(title: _title, image: img, selectedImage: img)
        nav.tabBarItem = item;
        return nav

    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
    }
    
    func setupViews(){
        let _content = UILabel()
        _content.text = self.title
        _content.textAlignment = NSTextAlignment.Center
        _content.setTranslatesAutoresizingMaskIntoConstraints(false)
        view.addSubview(_content)
        
        let _views = ["content":_content]
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("|-[content(>=100)]-|", options: NSLayoutFormatOptions(0), metrics: nil, views: _views))
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-100-[content]", options: NSLayoutFormatOptions(0), metrics: nil, views: _views))
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


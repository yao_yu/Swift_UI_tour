//
//  AboutViewController.swift
//  UI12-TabbarController
//
//  Created by yao_yu on 14-9-3.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {
    
    var _content = UIWebView()

    class func createView() ->AboutViewController{
        let this = AboutViewController()
        this.title = "关于"
        
        let img = UIImage(named: "collect_h@2x.png")
        let item = UITabBarItem(title: this.title, image: img, selectedImage: img)
        this.tabBarItem = item;
        return this
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
    }
    
    func setupViews(){
        
        var tb = UIView(frame: CGRectMake(0, 20, 200, 20))
        tb.setTranslatesAutoresizingMaskIntoConstraints(false)
        view.addSubview(tb)
        
        var btn = UIButton.buttonWithType(UIButtonType.System) as! UIButton
        btn.frame =  CGRectMake(20, 0, 100, 20)
//        btn.setTranslatesAutoresizingMaskIntoConstraints(false)
        btn.setTitle("加载", forState: UIControlState.Normal)
        btn.addTarget(self, action: "onLoad", forControlEvents: UIControlEvents.TouchUpInside)
        tb.addSubview(btn)
        
        var btn2 = UIButton.buttonWithType(UIButtonType.System) as! UIButton
        btn2.frame =  CGRectMake(120, 0, 100, 20)
        //btn2.setTranslatesAutoresizingMaskIntoConstraints(false)
        btn2.setTitle("重置", forState: UIControlState.Normal)
        btn2.addTarget(self, action: "onReset", forControlEvents: UIControlEvents.TouchUpInside)
        tb.addSubview(btn2)
        
        
        _content.backgroundColor = UIColor.brownColor()
        _content.setTranslatesAutoresizingMaskIntoConstraints(false)
        view.addSubview(_content)
        
        let _views = ["content":_content, "button":tb]
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[content(>=100)]|", options: NSLayoutFormatOptions(0), metrics: nil, views: _views))
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[button(>=100)]|", options: NSLayoutFormatOptions(0), metrics: nil, views: _views))
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-20-[button(20)]-[content(>=100)]-44-|", options: NSLayoutFormatOptions(0), metrics: nil, views: _views))

        onLoad()
    }

    func onReset(){
        _content.loadHTMLString("hello <h1>world<h1>", baseURL: nil)
    }
    
    func onLoad(){
        let request = NSURLRequest(URL: NSURL(string: "http://www.sina.com.cn")!)
        _content.loadRequest(request)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*这样操作内存有泄漏
    override func viewWillAppear(animated: Bool) {
        setupViews()
    }
    
    override func viewWillDisappear(animated: Bool) {
        for i in view.subviews{
            i.removeFromSuperview()
        }
    }
*/

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

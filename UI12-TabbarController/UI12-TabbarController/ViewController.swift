//
//  ViewController.swift
//  UI12-TabbarController
//
//  Created by yao_yu on 14-9-3.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITabBarControllerDelegate {
    
    var tabbar = UITabBarController()
                            
    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
        
        
    }
    
    func setupViews(){
        createTabbar()
    }

    func createTabbar(){
        var fulltitle = ""
        var controller = UIViewController()
        var views = [AnyObject]()
        var pages = [("主页", "recomment"),
            ("分类", "collect"),
            ("分级", "partition"),
            ("搜索", "search")
        ]
        for (_title, _file) in pages{            
            views.append(ViewControllerA.createNavigationController(_title, _file))
        }

        views.append(AboutViewController.createView())
        
        tabbar.viewControllers = views
        tabbar.hidesBottomBarWhenPushed = true
        tabbar.delegate = self
        
        view.addSubview(tabbar.view)
        
        func onOK(sender: AnyObject) {
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}


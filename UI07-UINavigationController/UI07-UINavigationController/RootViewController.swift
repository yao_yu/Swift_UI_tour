//
//  RootViewController.swift
//  UI07-UINavigationController
//
//  Created by yao_yu on 14-8-26.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//



import UIKit

class RootViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController.setToolbarHidden(false, animated: true)
        setupToolBar()
        
        view.backgroundColor = UIColor.whiteColor()
        
        let leftButton = UIBarButtonItem(barButtonSystemItem:UIBarButtonSystemItem.Action, target: self, action: "selectLeft")
        self.navigationItem.leftBarButtonItem  = leftButton
        
        let lb2 = UIBarButtonItem(title: "打开", style: UIBarButtonItemStyle.Bordered, target: self, action: "show2")
        self.navigationItem.leftBarButtonItems.append(lb2)
        
        let rightButton = UIBarButtonItem(barButtonSystemItem:UIBarButtonSystemItem.Add, target: self, action: "selectRight")
        self.navigationItem.rightBarButtonItem  = rightButton
        
        // 自定义标题
        let parray = ["鸡翅", "牛排"]
        var seg = UISegmentedControl(items: parray)
        seg.addTarget(self, action:"segmentAction:", forControlEvents:UIControlEvents.ValueChanged)
        self.navigationItem.titleView = seg
    }
    
    func setupToolBar(){
        var buttons = [UIBarButtonItem]()
        buttons.append(UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Add, target: nil, action: nil))
        buttons.append(UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Bookmarks, target: nil, action: nil))
        buttons.append(UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Edit, target: nil, action: nil))
        buttons.append(UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Camera, target: nil, action: nil))
        buttons.append(UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Pause, target: nil, action: nil))
        self.setToolbarItems(buttons, animated: true)
    }
    
    func selectLeft(){
        let alter = UIAlertView(title: "提示", message: "你点击了导航栏左按钮", delegate: self, cancelButtonTitle: "确定")
        alter.show()
    }
    
    func selectRight(){
        let alter = UIAlertView(title: "提示", message: "你点击了导航栏右按钮", delegate: self, cancelButtonTitle: "确定")
        alter.show()
    }
    
    func show2(){
        let secondView = SecondViewController()
        self.navigationController.pushViewController(secondView, animated: true)
        secondView.title = "第二视图"
    }
    
    func segmentAction(sender:AnyObject!){
        let seg = sender as UISegmentedControl
        switch seg.selectedSegmentIndex{
        case 0:
            let alter = UIAlertView(title: "提示", message: "点了鸡翅", delegate: self, cancelButtonTitle: "确定")
            alter.show()
        case 1:
            let alter = UIAlertView(title: "提示", message: "点了牛排", delegate: self, cancelButtonTitle: "确定")
            alter.show()
        default:
            break
            
        }
    }
    
}
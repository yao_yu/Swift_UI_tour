//
//  SecondViewController.swift
//  UI07-UINavigationController
//
//  Created by yao_yu on 14-8-26.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    
    override func viewDidLoad() {

        view.backgroundColor = UIColor.whiteColor()
//        let backButton = UIBarButtonItem(title: "==主界面", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
//        self.navigationItem.backBarButtonItem = backButton
        
        var button = UIButton.buttonWithType(UIButtonType.System) as UIButton
        button.setTitle("自定义标题", forState: UIControlState.Normal)
        button.sizeToFit()
        self.navigationItem.titleView = button
        self.navigationItem.title = "中华人民共和国"
        
        self.navigationController.setToolbarHidden(true, animated: true)
        setupToolBar()
        
    }
    
    func setupToolBar(){
        var buttons = [UIBarButtonItem]()
        buttons.append(UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Play, target: nil, action: nil))
        buttons.append(UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Redo, target: nil, action: nil))
        buttons.append(UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Search, target: nil, action: "goto3"))
        let toolbar = UIToolbar(frame: CGRectMake(0,  self.view.frame.size.height -    44.0, self.view.frame.size.width, 44.0))
        toolbar.setItems(buttons, animated: true)
        view.addSubview(toolbar)
    }
    
    func goto3(){
        let vc = ThirdViewController()
        self.navigationController.pushViewController(vc, animated: true)
        vc.title = "第三视图"
    }

}

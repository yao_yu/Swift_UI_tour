//
//  ViewController.swift
//  UI07-UINavigationController
//
//  Created by yao_yu on 14-8-26.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var navController = UINavigationController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
    }
    
    func setupViews(){
        view.addSubview(navController.view)
        
        let rootView = RootViewController()
        rootView.title = "根视图"
        
        navController.pushViewController(rootView, animated: true)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}


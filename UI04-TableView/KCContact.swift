//
//  KCContact.swift
//  UI04-TableView
//
//  Created by yao_yu on 14-8-24.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import Foundation

// 联系人
class KCContact{
    var firstName = ""
    var lastName = ""
    var phoneNumber = ""
    
    var name:String{
        return "\(firstName)\(lastName)"
    }
    
    init(firstName:String, lastName:String){
        self.firstName = firstName
        self.lastName = lastName
    }
    
    init(firstName:String, lastName:String, phoneNumber:String){
        self.firstName = firstName
        self.lastName = lastName
        self.phoneNumber = phoneNumber
    }
}

// 联系人分组
class KCContactGroup{
    var name = ""
    var detail = ""
    var contacts = [KCContact]()
    
    init(name:String, detail:String, contacts:[KCContact]){
        self.name = name
        self.detail = detail
        self.contacts = contacts
    }
}
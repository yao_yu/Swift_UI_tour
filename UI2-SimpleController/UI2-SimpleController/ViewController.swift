//
//  ViewController.swift
//  UI2-SimpleController
//
//  Created by yao_yu on 14-8-21.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var label:UILabel?
                            
    override func viewDidLoad() {
        super.viewDidLoad()

        // 创建标签
        label = UILabel()
        let _label = label!
        _label.textAlignment = NSTextAlignment.Center
        _label.text = "欢迎来到Swfit世界!"
        _label.setTranslatesAutoresizingMaskIntoConstraints(false)
        self.view.addSubview(_label)
        
        let views = ["MyLabel":_label]
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("|-[MyLabel]-|", options: NSLayoutFormatOptions(0), metrics: nil, views: views))
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-[MyLabel]-|", options: NSLayoutFormatOptions(0), metrics: nil, views: views))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}


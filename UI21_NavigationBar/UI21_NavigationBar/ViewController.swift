//
//  ViewController.swift
//  UI21_NavigationBar
//
//  Created by yao_yu on 14-9-24.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var naviBar: UINavigationBar!
    @IBOutlet weak var leftBarItem: UIBarButtonItem!
    
    @IBAction func onOK(sender: AnyObject) {
        let alert = UIAlertView(title: "中华人民共和国", message: "艺术硕士苦", delegate: nil, cancelButtonTitle: "确定")
        alert.show()
    }
    
    func buttonClick(sender: AnyObject) {
        onOK(sender)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        let barItem:UIBarButtonItem = UIBarButtonItem(title: "添加", style: UIBarButtonItemStyle.Plain, target: self, action: "buttonClick:")
        naviBar.items[0].leftBarButtonItem = barItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}


//
//  AppDelegate.h
//  UI24_LBS_ObjC
//
//  Created by yao_yu on 14-9-25.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


//
//  main.m
//  UI24_LBS_ObjC
//
//  Created by yao_yu on 14-9-25.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

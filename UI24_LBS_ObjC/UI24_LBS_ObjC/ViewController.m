//
//  ViewController.m
//  UI24_LBS_ObjC
//
//  Created by yao_yu on 14-9-25.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

#import "ViewController.h"
#import <MapKit/MapKit.h>

@interface ViewController ()

@property (weak, nonatomic) IBOutlet MKMapView *map;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    MKCoordinateSpan aSpan = MKCoordinateSpanMake(0.05, 0.05);
    CLLocationCoordinate2D Center = CLLocationCoordinate2DMake(28.19, 112.98);
    MKCoordinateRegion region = MKCoordinateRegionMake(Center, aSpan);
    
    [self.map setRegion:region animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

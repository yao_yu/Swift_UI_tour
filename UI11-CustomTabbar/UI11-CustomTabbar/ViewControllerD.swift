//
//  ViewControllerD.swift
//  UI11-CustomTabbar
//
//  Created by yao_yu on 14-9-3.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import UIKit

class ViewControllerD: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // 添加标签
        let label = UILabel(frame: CGRectMake(0, 60, 320, 44))
        label.textAlignment = NSTextAlignment.Center
        label.text = "视图D"
        view.addSubview(label)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

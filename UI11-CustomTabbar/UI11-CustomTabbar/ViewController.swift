//
//  ViewController.swift
//  UI11-CustomTabbar
//
//  Created by yao_yu on 14-9-2.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
                            
    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
    }
    
    func setupViews(){
        let v1 = ViewControllerA()
        v1.hidesBottomBarWhenPushed = false
        v1.title = "首页"
        let v2 = ViewControllerB()
        v2.hidesBottomBarWhenPushed = false
        v2.title = "搜索"
        let v3 = ViewControllerC()
        v3.hidesBottomBarWhenPushed = false
        v3.title = "我"
        let v4 = ViewControllerD()
        v4.hidesBottomBarWhenPushed = false
        v4.title = "设置"
        
        let nav1 = UINavigationController(rootViewController: v1)
        let nav2 = UINavigationController(rootViewController: v2)
        let nav3 = UINavigationController(rootViewController: v3)
        let nav4 = UINavigationController(rootViewController: v4)
        
        let tabBarController = UITabBarController()
        tabBarController.viewControllers = [nav1, nav2, nav3, nav4]
        tabBarController.selectedIndex = 0
        
        view.addSubview(tabBarController.view)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}


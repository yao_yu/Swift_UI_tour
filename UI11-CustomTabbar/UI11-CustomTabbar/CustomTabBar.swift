//
//  CustomTabBar.swift
//  UI11-CustomTabbar
//
//  Created by yao_yu on 14-9-2.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import UIKit

class CustomTabBar: UITabBarController {
    
    var buttons = [UIButton]()
    var currentSelectedIndex = -1
    var slideBg = UIImageView()

    override func viewDidAppear(animated: Bool) {
        slideBg = UIImageView(image: UIImage(named: "bottomfocus.png"))
        hideRealTabBar()
        customTabBar()
    }
    
    func hideRealTabBar(){
        for subview in view.subviews{
            subview.removeFromSuperview()
        }
    }
    
    func customTabBar(){
        let imgView = UIImageView(image: UIImage(named: "tabbg2.png"))
        imgView.frame = CGRectMake(0, 425, imgView.image.size.width, imgView.image.size.height)
        view.addSubview(imgView)
        
        slideBg.frame = CGRectMake(-30, self.tabBar.frame.origin.y,  slideBg.image.size.width, slideBg.image.size.height)

        let viewCount = self.viewControllers.count > 5 ? 5 : self.viewControllers.count
        self.buttons = [UIButton]()
        let _width = 320 / viewCount
        let _height = self.tabBar.frame.size.height
        for i in 0..<viewCount{
            var btn = UIButton.buttonWithType(UIButtonType.System) as UIButton
            btn.frame = CGRectMake(CGFloat(i * _width), self.tabBar.frame.origin.y, CGFloat(_width), CGFloat(_height))
            btn.addTarget(self, action: "selectedTab:", forControlEvents: UIControlEvents.TouchUpInside)
            btn.tag = i
            self.view.addSubview(btn)
        }
        
        self.view.addSubview(slideBg)
        let imgFront = UIImageView(image: UIImage(named: "tabitem.png"))
        imgFront.frame = imgView.frame
        self.view.addSubview(imgFront)
        
        selectedTab(buttons[0])
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        self.tabBar.hidden=true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func selectedTab(button:UIButton){
        if self.selectedIndex == button.tag{
            return
        }
        self.selectedIndex = button.tag
        self.currentSelectedIndex = button.tag

    }
    
    func slideTabBg(btn:UIButton){
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.2)
        UIView.setAnimationDelegate(self)
        slideBg.frame = CGRectMake(btn.frame.origin.x - 30, btn.frame.origin.y, slideBg.image.size.width, slideBg.image.size.height)
        UIView.commitAnimations()
    }
    
//    - (void)slideTabBg:(UIButton *)btn{
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:0.20];
//    [UIView setAnimationDelegate:self];
//    slideBg.frame = CGRectMake(btn.frame.origin.x - 30, btn.frame.origin.y, slideBg.image.size.width, slideBg.image.size.height);
//    [UIView commitAnimations];
//    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

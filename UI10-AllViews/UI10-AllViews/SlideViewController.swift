//
//  SlideViewController.swift
//  UI10-AllViews
//
//  Created by yao_yu on 14-8-29.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import UIKit

class SliderViewController: UIViewController {
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var slider = UISlider(frame:CGRectMake(10.0, 150.0, 300.0, 30.0))
        slider.addTarget(self, action:"sliderAction:", forControlEvents:UIControlEvents.ValueChanged)
        self.view.addSubview(slider)
    }
    
    func sliderAction(sender: UISlider) {
        println("sliderAction:\(sender.value)")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
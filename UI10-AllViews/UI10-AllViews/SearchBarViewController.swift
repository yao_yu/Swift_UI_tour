//
//  SearchBarViewController.swift
//  UI10-AllViews
//
//  Created by yao_yu on 14-8-29.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import UIKit

class SearchBarViewController: UIViewController, UISearchBarDelegate {
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var searchBar = UISearchBar(frame:CGRectMake(0, 100.0, 320.0, 200.0))
        searchBar.showsCancelButton = true
        searchBar.searchBarStyle = UISearchBarStyle.Default
        searchBar.scopeButtonTitles = [
            "scope A",
            "scope B"
        ]
        searchBar.showsScopeBar = true
        
        self.view.addSubview(searchBar)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //    UISearchBarDelegate
    func searchBar(UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        println("selectedScopeButtonIndexDidChange \(selectedScope)")
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        println("searchBarSearchButtonClicked: \(searchBar.text)")
        
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        println("searchBarCancelButtonClicked")
        searchBar.resignFirstResponder()
    }
    
}

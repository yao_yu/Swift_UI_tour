//
//  YYAlertViewController.swift
//  UI10-AllViews
//
//  Created by yao_yu on 14-8-29.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import UIKit

class AlertViewController: UIViewController {
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidAppear(animated: Bool) {
        
        let alertController = UIAlertController(title: "title", message: "message", preferredStyle: UIAlertControllerStyle.Alert)
        
        let cancelAction = UIAlertAction(title: "cancel", style: UIAlertActionStyle.Cancel) { action in
            println("cancel")
        }
        
        let otherAction = UIAlertAction(title: "other", style: UIAlertActionStyle.Default) { action in
            NSLog("other")
        }
        
        let secondAction = UIAlertAction(title: "secondAction", style: UIAlertActionStyle.Default) { action in
            NSLog("secondAction")
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(otherAction)
        alertController.addAction(secondAction)
        
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
//
//  WebViewController.swift
//  UI10-AllViews
//
//  Created by yao_yu on 14-8-29.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import UIKit

class WebViewController: UIViewController, UIWebViewDelegate {
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var webView = UIWebView(frame: self.view.bounds)
        webView.backgroundColor = UIColor.whiteColor()
        webView.scalesPageToFit = true
        webView.delegate = self;
        
        var url = NSURL(string: "http://www.baidu.com")
        var request = NSURLRequest(URL: url!)
        
        webView.loadRequest(request)
        
        self.view.addSubview(webView)
    }
    
    //    UIWebViewDelegate
    func webViewDidStartLoad(webView: UIWebView) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
    }
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError) {
        println("didFailLoadWithError")
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
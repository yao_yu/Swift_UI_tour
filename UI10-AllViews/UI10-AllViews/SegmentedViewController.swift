//
//  SegmentedViewController.swift
//  UI10-AllViews
//
//  Created by yao_yu on 14-8-29.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import UIKit

class SegmentedViewController: UIViewController {
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var segment = UISegmentedControl(items:["one", "two", "three", "four"])
        segment.frame = CGRectMake(10.0, 110.0, 300.0, 30.0)
//        segment.segmentedControlStyle 
        segment.momentary = true
        segment.addTarget(self, action:"segmentAction:", forControlEvents:UIControlEvents.TouchUpInside)
        
        self.view.addSubview(segment)
    }
    
    func segmentAction(sender: UISegmentedControl) {
        println("segmentAction")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
//
//  ProgressViewController.swift
//  UI10-AllViews
//
//  Created by yao_yu on 14-8-29.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import UIKit

class ProgressViewController: UIViewController {
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidAppear(animated: Bool) {
        var progress = UIProgressView(progressViewStyle:UIProgressViewStyle.Default)
        progress.progressTintColor = UIColor.blackColor()
        progress.trackTintColor  = UIColor.redColor()
        progress.frame = CGRectMake(10.0, 150.0, 300.0, 40.0)
        progress.setProgress(1, animated: true)
        self.view.addSubview(progress)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//
//  DatePickerViewController.swift
//  UI10-AllViews
//
//  Created by yao_yu on 14-8-29.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import UIKit

class DatePickerViewController: UIViewController {
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var datePicker = UIDatePicker(frame:CGRectMake(0.0, 120.0, 200.0, 200.0))
        datePicker.datePickerMode = UIDatePickerMode.Date
        datePicker.minimumDate = NSDate()
        datePicker.minuteInterval = 1
        datePicker.addTarget(self, action: "action", forControlEvents: UIControlEvents.ValueChanged)
        
        self.view.addSubview(datePicker)
    }
    
    func action() {
        println("action")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
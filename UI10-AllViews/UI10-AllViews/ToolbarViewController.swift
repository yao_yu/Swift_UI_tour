//
//  ToolbarViewController.swift
//  UI10-AllViews
//
//  Created by yao_yu on 14-8-29.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import UIKit

class ToolbarViewController: UIViewController {
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var toolBar = UIToolbar(frame:CGRectMake(10.0, 120.0, 300.0, 30.0))
        toolBar.barStyle = .BlackTranslucent
        toolBar.tintColor = UIColor.greenColor()
        toolBar.backgroundColor = UIColor.blueColor()
        
        var flexibleSpace = UIBarButtonItem(barButtonSystemItem:UIBarButtonSystemItem.FlexibleSpace, target:"barButtonItemClicked:", action:nil)
        var barBtnItemA = UIBarButtonItem(title: "one", style:UIBarButtonItemStyle.Plain, target:self, action:"barButtonItemClicked:")
        var barBtnItemB = UIBarButtonItem(title: "two", style:UIBarButtonItemStyle.Plain, target:self, action:"barButtonItemClicked:")
        var barBtnItemC = UIBarButtonItem(title: "three", style:UIBarButtonItemStyle.Plain, target:self, action:"barButtonItemClicked:")
        var barBtnItemD = UIBarButtonItem(title: "four", style:UIBarButtonItemStyle.Plain, target:self, action:"barButtonItemClicked:")
        
        toolBar.items = [flexibleSpace, barBtnItemA, flexibleSpace, barBtnItemB, flexibleSpace, barBtnItemC, flexibleSpace, barBtnItemD, flexibleSpace]
        
        self.view.addSubview(toolBar)
    }
    
    func barButtonItemClicked(sender: UIBarButtonItem) {
        NSLog("barButtonItemClicked: \(sender.title)")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//
//  PageViewController.swift
//  UI10-AllViews
//
//  Created by yao_yu on 14-8-29.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import UIKit

class PageControlViewController: UIViewController {
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var pageControl = UIPageControl(frame: CGRectMake(0, 100, 320, 400));
        pageControl.backgroundColor = UIColor.blueColor()
        pageControl.numberOfPages = 10
        pageControl.currentPage = 5
        pageControl.currentPageIndicatorTintColor = UIColor.grayColor()
        pageControl.pageIndicatorTintColor = UIColor.redColor()
        pageControl.addTarget(self, action: "pageControlAction:", forControlEvents:UIControlEvents.ValueChanged)
        
        self.view.addSubview(pageControl)
    }
    
    func pageControlAction(sender: UIPageControl) {
        println("pageControlAction:\(sender.currentPage)")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//
//  ActivityIndicatorViewController.swift
//  UI10-AllViews
//
//  Created by yao_yu on 14-8-29.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import UIKit

class ActivityIndicatorViewController: UIViewController {
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidAppear(animated: Bool) {
        
        var activity = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        activity.frame = CGRectMake(100, 100, 40, 40)
        activity.startAnimating()
        activity.hidesWhenStopped = true
        
        self.view.addSubview(activity)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
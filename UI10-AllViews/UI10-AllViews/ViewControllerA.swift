//
//  ViewControllerA.swift
//  UI10-AllViews
//
//  Created by yao_yu on 14-8-28.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import UIKit

class ViewControllerA: UITableViewController {

    var list:NSArray?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.list = NSArray(contentsOfFile: NSBundle.mainBundle().pathForResource("BasicUI", ofType: "plist")!)
        
        self.tableView!.registerClass(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return list!.count
    }
    
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! UITableViewCell

        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        cell.textLabel!.text = list!.objectAtIndex(indexPath.row) as? String

        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        var itemString = list!.objectAtIndex(indexPath.row) as! String
        var viewController:UIViewController?
        switch itemString{
        case "UIView":
            viewController = TheViewController()
        case "UILabel":
            viewController = LabelViewController()
        case "UIButton":
            viewController = ButtonViewController()
        case "UIImageView":
            viewController = ImageViewController()
        case "UITextField":
            viewController = TextFieldViewController()
        case "UITextView":
            viewController = TextViewController()
        case "UISwitch":
            viewController = YYSwitchViewController()
        case "UIScrollView":
            viewController = ScrollViewController()
        case "UIPageControl":
            viewController = PageControlViewController()
        case "UIAlertView/UIActionSheet":
            viewController = AlertViewController()
        case "UIActivityIndicatorView":
            viewController = ActivityIndicatorViewController()
        case "UISlide":
            viewController = SliderViewController()
        case "UIProgressView":
            viewController = ProgressViewController()
        case "UISegmentedControl":
            viewController = SegmentedViewController()
        case "UIDatePicker":
            viewController = DatePickerViewController()
        case "UIWebView":
            viewController = WebViewController()
        case "UIToolbar":
            viewController = ToolbarViewController()
        case "UISearchBar":
            viewController = SearchBarViewController()
        default:
            println(itemString)
            return
        }
        viewController!.hidesBottomBarWhenPushed = true
        self.navigationController!.pushViewController(viewController!, animated:true)
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView!, canEditRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView!, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath!) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView!, moveRowAtIndexPath fromIndexPath: NSIndexPath!, toIndexPath: NSIndexPath!) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView!, canMoveRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}

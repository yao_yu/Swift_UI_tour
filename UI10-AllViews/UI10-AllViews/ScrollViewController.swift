//
//  ScrollViewController.swift
//  UI10-AllViews
//
//  Created by yao_yu on 14-8-29.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import UIKit

import UIKit

class ScrollViewController: UIViewController, UIScrollViewDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var scroll = UIScrollView(frame: self.view.bounds)
        scroll.pagingEnabled = false
        scroll.scrollEnabled = true
        scroll.showsVerticalScrollIndicator = true
        scroll.showsHorizontalScrollIndicator = true
        scroll.delegate = self
        
        var X : CGFloat = 0
        let n = 4
        for (var i = 0; i < n; i++) {
            var label = UILabel()
            label.text = "\(i)"
            label.textAlignment = NSTextAlignment.Center
            label.backgroundColor = UIColor.lightGrayColor()
            label.frame = CGRectMake(X, 0, self.view.bounds.size.width, 400)
            X += 320
            scroll.addSubview(label)
        }
        
        scroll.contentSize = CGSizeMake(CGFloat(n) * self.view.bounds.size.width, 400)
        
        self.view.addSubview(scroll)
    }
    
    //    UIScrollViewDelegate
    func scrollViewDidScroll(scrollView: UIScrollView) {
        println("scrollViewDidScroll")
    }
    
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
    }
    
    func scrollViewWillEndDragging(scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
    }
    
    
    func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
    }
    
    func scrollViewWillBeginDecelerating(scrollView: UIScrollView) {
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
//
//  ChildViewControllers.swift
//  UI10-AllViews
//
//  Created by yao_yu on 14-8-28.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import UIKit

class TheView:UIView{
    override func drawRect(rect: CGRect) {
        UIColor.redColor().setFill()
        UIRectFill(CGRectMake(100, 100, 100, 100))
    }
}

class TheViewController:UIViewController{
    override func viewDidLoad() {
        super.viewDidLoad()

        let theView = TheView()
        theView.backgroundColor = UIColor.brownColor()
        theView.layer.cornerRadius = 30
        theView.layer.borderWidth = 5
        theView.layer.borderColor = UIColor.darkGrayColor().CGColor
        
        self.view = theView;

        //        添加手势
        let recognizer = UITapGestureRecognizer(target: self, action: "tapAction:")
        recognizer.numberOfTapsRequired = 2;
        recognizer.numberOfTouchesRequired = 1;
        self.view.addGestureRecognizer(recognizer)
    }
    
    func tapAction(sender: UIGestureRecognizer)
    {
        println("tapAction")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

class LabelViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var label = UILabel(frame: CGRectMake(20, 80, self.view.bounds.size.width-40, 200))
        label.backgroundColor = UIColor.blackColor()
        label.textColor = UIColor.whiteColor()
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.ByWordWrapping
        label.text = "Swift is designed to provide seamless compatibility with Cocoa and Objective-C. You can use Objective-C APIs (ranging from system frameworks to your own custom code) in Swift, and you can use Swift APIs in Objective-C. "
        label.font = UIFont.italicSystemFontOfSize(20)
        
        self.view.addSubview(label)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

class ButtonViewController:UIViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var button = UIButton.buttonWithType(UIButtonType.System) as! UIButton
        button.frame = CGRectMake(100, 200, 100, 100)
        button.setTitleColor(UIColor.redColor(), forState: UIControlState.Normal)
        button.setTitleColor(UIColor.blueColor(), forState: UIControlState.Selected)
        button.setTitle("正常", forState: UIControlState.Normal)
        button.setTitle("选中", forState: UIControlState.Selected)
        button.tag = 1000
        button.clipsToBounds = true
//        button.backgroundColor = UIColor.grayColor()
        button.layer.cornerRadius = 5
        button.addTarget(self, action: "onOK", forControlEvents: UIControlEvents.TouchUpInside)
        
        view.addSubview(button)
    }
    
    func onOK(){
        println("onOK")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

class ImageViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var imageView = UIImageView(frame: CGRectMake(10, 30, 300, 500))
        imageView.backgroundColor = UIColor.whiteColor();
        imageView.animationImages = [
            UIImage(contentsOfFile: NSBundle.mainBundle().pathForResource("001", ofType: "jpg")!)!,
            UIImage(contentsOfFile: NSBundle.mainBundle().pathForResource("001", ofType:"jpg")!)!,
            UIImage(contentsOfFile: NSBundle.mainBundle().pathForResource("002", ofType:"jpg")!)!,
            UIImage(contentsOfFile: NSBundle.mainBundle().pathForResource("003", ofType:"jpg")!)!,
            UIImage(named: "004.jpg")!
        ]
        imageView.contentMode = UIViewContentMode.ScaleAspectFit
        imageView.animationDuration = 3;
        imageView.startAnimating()
        
        self.view.addSubview(imageView)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

class TextFieldViewController: UIViewController, UITextFieldDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "textFieldTextDidBeginEditingNotification:", name:UITextFieldTextDidBeginEditingNotification, object:nil)
        
        var textField = UITextField(frame: CGRectMake(10, 100, 300, 40))
        textField.backgroundColor = UIColor.clearColor()
        textField.textColor = UIColor.blackColor()
        textField.placeholder = "请输入..."
        textField.borderStyle = UITextBorderStyle.RoundedRect
        textField.adjustsFontSizeToFitWidth = true
        textField.delegate = self
        
        self.view.addSubview(textField)
    }
    
    func textFieldTextDidBeginEditingNotification(sender: NSNotification!) {
        
    }
    
    //    UITextFieldDelegate
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        return true;
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        return true;
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        return true;
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        return true;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

class TextViewController: UIViewController, UITextViewDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var textView = UITextView(frame:CGRectMake(10.0, 120.0, 300.0, 400.0))
        textView.backgroundColor = UIColor.blackColor()
        textView.textColor = UIColor.whiteColor()
        textView.editable = true
        textView.font = UIFont.boldSystemFontOfSize(30)
        textView.delegate = self;
        textView.text = "Swift is designed to provide seamless compatibility with Cocoa and Objective-C. You can use Objective-C APIs (ranging from system frameworks to your own custom code) in Swift, and you can use Swift APIs in Objective-C. "
        self.view.addSubview(textView)
    }
    
    func textViewShouldBeginEditing(textView: UITextView) -> Bool {
        return true;
    }
    
    func textViewShouldEndEditing(textView: UITextView) -> Bool {
        return true;
    }
    
    func textViewDidBeginEditing(textView: UITextView) {
    }
    
    func textViewDidEndEditing(textView: UITextView) {
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        return true;
    }
    
    func textViewDidChange(textView: UITextView) {
    }
    
    func textViewDidChangeSelection(textView: UITextView) {
    }
    
    func textView(textView: UITextView, shouldInteractWithURL URL: NSURL, inRange characterRange: NSRange) -> Bool {
        return true;
    }
    
    func textView(textView: UITextView, shouldInteractWithTextAttachment textAttachment: NSTextAttachment, inRange characterRange: NSRange) -> Bool {
        return true;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

class YYSwitchViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var switchCtr = UISwitch(frame: CGRectMake(100, 100, 80, 30))
        switchCtr.on = true
        switchCtr.addTarget(self, action: "switchAction:", forControlEvents:UIControlEvents.ValueChanged)
        self.view.addSubview(switchCtr)
    }
    
    func switchAction(sender: UISwitch) {
        println("switchAction: \(sender.on)")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
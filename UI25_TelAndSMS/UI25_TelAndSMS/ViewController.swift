//
//  ViewController.swift
//  UI25_TelAndSMS
//
//  Created by yao_yu on 14-9-26.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var txtPhoneNo: UITextField!
    @IBOutlet weak var txtSMSContent: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onCall(sender: AnyObject) {
        println("tel://\(txtPhoneNo.text)")
        UIApplication.sharedApplication().openURL(NSURL(string: "tel://\(txtPhoneNo.text)"))
    }


    @IBAction func onSendSMS(sender: AnyObject) {
        
    }
}


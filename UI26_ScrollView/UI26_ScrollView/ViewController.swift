//
//  ViewController.swift
//  UI26_ScrollView
//
//  Created by yao_yu on 14/11/7.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIScrollViewDelegate {
    
    var navbarView:UIView!
    
    var scrollTitle:UIScrollView!
    var navbarTitles: [UILabel]!
    var pageViews:[UIView]!
    
    var scrollView:UIScrollView!
    var pages = 40
    
    var pageControlView:UIPageControl!
    
    var pageIndex = 0
    let titleWidth:CGFloat = 160

    override func viewDidLoad() {
        super.viewDidLoad()

        //自定义导航条
        navbarView = UIView()
        navbarView.backgroundColor = UIColor.clearColor()
        self.navigationController?.navigationBar.addSubview(navbarView)
        
        scrollTitle = UIScrollView()
        scrollTitle.frame = CGRectMake(0, 0, titleWidth, 44)
        scrollTitle.pagingEnabled = true
        scrollTitle.showsHorizontalScrollIndicator = false
//        scrollTitle.scrollEnabled = false
        scrollTitle.contentSize = CGSize(width: titleWidth * CGFloat(self.pages), height: 44)
        scrollTitle.backgroundColor = UIColor.clearColor()
        navbarView.addSubview(scrollTitle)
        
        pageControlView = UIPageControl()
        pageControlView.frame = CGRectMake(-titleWidth/2, 30, titleWidth, 0)
        pageControlView.backgroundColor = UIColor.whiteColor()
        pageControlView.numberOfPages = pages
        pageControlView.currentPage = pageIndex
        pageControlView.currentPageIndicatorTintColor = UIColor(red:0.325, green:0.667, blue:0.922, alpha: 1)
        pageControlView.pageIndicatorTintColor = UIColor.whiteColor()


        navbarView.addSubview(pageControlView)
        
        navbarTitles = [UILabel]()
        for i in 0..<pages{
            navbarTitles.append(UILabel())
            navbarTitles[i].text = "第\(i+1)页"
            navbarTitles[i].textAlignment = NSTextAlignment.Center
            navbarTitles[i].frame = CGRectMake(CGFloat(i) * titleWidth, 0, titleWidth, 22)
            scrollTitle.addSubview(navbarTitles[i])
        }
        
        //滚动视图
        scrollView = UIScrollView()
        scrollView.backgroundColor = UIColor.clearColor()
        scrollView.pagingEnabled = true
        scrollView.delegate = self
        self.view.addSubview(scrollView)
        
        //在滚动视图中添加页面视图
        pageViews = [UIView]()
        for i in 0..<pages{
            pageViews.append(UIView())
            pageViews[i].backgroundColor = UIColor(red: myRand, green: myRand, blue: myRand, alpha: 1)
            scrollView.addSubview(pageViews[i])
        }
    }
    
    var myRand: CGFloat{
        return CGFloat(random() % self.pages) / CGFloat(self.pages)
    }
    
    override func viewDidLayoutSubviews() {
        
        navbarView.frame = CGRectMake((view.bounds.size.width-titleWidth)/2, 0, titleWidth, 44)
        
        scrollView.frame = view.frame
        scrollView.contentSize = CGSize(width: self.view.bounds.size.width * CGFloat(pages), height: self.view.bounds.size.height/2)
        
        for i in 0..<pages{
            pageViews[i].frame = CGRect(x: CGFloat(i) * view.frame.width, y: 0.0, width: scrollView.frame.width, height: scrollView.frame.height)
        }
        
        scrollView.scrollRectToVisible(CGRect(x: CGFloat(self.pageIndex) * view.frame.width, y: 0.0, width: scrollView.frame.width, height: scrollView.frame.height), animated: true)

    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let xOffset = scrollView.contentOffset.x
        // = Int(xOffset / view.bounds.width)
        
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let xOffset = scrollView.contentOffset.x
        pageIndex = Int(xOffset / view.bounds.width)
        scrollTitle.scrollRectToVisible(CGRectMake(CGFloat(pageIndex) * titleWidth, 0, titleWidth, 30), animated: true)
        pageControlView.currentPage = pageIndex
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}


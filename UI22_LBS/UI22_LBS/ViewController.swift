//
//  ViewController.swift
//  UI22_LBS
//
//  Created by yao_yu on 14-9-25.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import UIKit

class ViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var lblLBS: UILabel!
    
    let locationManager = CLLocationManager()   //用于获取位置
    //var locationManager:CLLocationManager?      //用于获取位置
    var checkinLocation:CLLocation?             //用于保存获取到的位置信息

    override func viewDidLoad() {
        super.viewDidLoad()
        setupLBS()
    }
    
    func setupLBS(){
        if CLLocationManager.locationServicesEnabled(){
            locationManager.delegate = self
            locationManager.distanceFilter = 200
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
            println("启动位置服务")
        } else {
            println("无法位置服务")
        }
        
    }

    @IBAction func WhereAmI(sender: AnyObject) {
        //setupLBS()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateToLocation newLocation: CLLocation!, fromLocation oldLocation: CLLocation!) {
        checkinLocation = newLocation
        lblLBS.text = newLocation.description
        
        println((newLocation.coordinate.latitude,newLocation.coordinate.longitude))
    }
    
    func displayLocationInfo(placemark: CLPlacemark) {
        //if placemark != nil {
            //stop updating location to save battery life
            locationManager.stopUpdatingLocation()
//        }
    }
    
    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
        println("Error while updating location " + error.localizedDescription)
    }

}

